"use strict";

const { alphabetise, getWildcardCount, checkDictionary } = require("./utils");

let mockDictionary = [
  "Elephant",
  "Hawk",
  "Racecar",
  "Foreward",
  "Rewind",
  "Haptene",
  "heeltap",
  "heptane"
];

test("order the word elephant to aeehlnpt", () => {
  expect(alphabetise("elephant")).toBe("aeehlnpt");
});

test("order the word ZZZzzzAAA to aaazzzzzz", () => {
  expect(alphabetise("ZZZzzzAAA")).toBe("aaazzzzzz");
});

test("disallow null (or empty string)", () => {
  expect(alphabetise(null)).toBe("");
});

test("wildcard count of 2", () => {
  expect(getWildcardCount("dsfj?sdfs?ssdfsdf")).toBe(2);
});

test("wildcard count of 0", () => {
  expect(getWildcardCount("dsfjssdfsdf")).toBe(0);
});
/////////////////////////////////////////

test("mock dictionary should have 1 anagram", () => {
  const results = checkDictionary("werind", mockDictionary);
  expect(results["6"]).toHaveLength(1);
});

test("mock dictionary should have 4 out of 5 anagrams", () => {
  const results = checkDictionary("elephant", mockDictionary);
  expect(results["8"]).toHaveLength(1);
  expect(results["7"]).toHaveLength(3);
});

test("string with 1 wildcard - 1 result", () => {
  const results = checkDictionary("h?ka", mockDictionary);
  expect(results["4"]).toHaveLength(1);
});

test("string with 2 wildcards - 2 results", () => {
  const results = checkDictionary("F?r?ward", mockDictionary);
  expect(results["8"]).toHaveLength(1);
  expect(results["4"]).toHaveLength(1);
});

test("12 ? input - return empty object", () => {
  const results = checkDictionary("????????????", mockDictionary);
  expect(results).toHaveLength(0);
});
