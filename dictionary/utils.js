"use strict";

var fs = require("fs");
const path = require("path");
const dictionaryFile = "./assets/enable1-wwf-v4.0-wordlist.txt";
let wwfDictionary;
fs.readFile(
  path.resolve(__dirname, dictionaryFile),
  "utf8",
  (err, contents) => {
    if (err) {
      throw err;
    }
    wwfDictionary = contents.split("\n");
  }
);

const alphabetise = word => {
  if (!word || typeof word == "undefined") {
    return "";
  }
  return word
    .toLowerCase()
    .split("")
    .sort()
    .join("");
};

const getWildcardCount = word => {
  let count = 0;
  for (const c of word.split("")) {
    if (c == "?") {
      count++;
    }
  }
  return count;
};

const validateInput = inputString => {
  const valRegex = /^((\?{0}[a-z]{2,12})|(\?{1}[a-z]{1,11})|(\?{2}[a-z]{1,10}))$/i;
  return valRegex.test(inputString);
};

const checkDictionary = (inputString, dictionary = wwfDictionary) => {
  const inputAlphabetise = alphabetise(inputString.toLowerCase());
  const results = [];

  // not a valid search term - return empty array
  if (!validateInput(inputAlphabetise)) {
    return results;
  }

  const wCount = getWildcardCount(inputString);

  for (var i = 0; i < dictionary.length; i++) {
    let wildCardCount = wCount;
    const word = dictionary[i].toLowerCase();
    // if word length > search term length skip
    if (word.length > inputAlphabetise.length) {
      continue;
    }

    // if exact match, add to results
    if (alphabetise(word) === inputAlphabetise) {
      results.push(word);
      continue;
    }
    // loop through each letter of word
    let inputCopy = inputAlphabetise;
    let isMatch = true;
    for (var j = 0; j < word.length; j++) {
      const thisChar = word[j];
      const indx = inputCopy.indexOf(thisChar);
      if (indx > -1) {
        inputCopy = stripAChar(inputCopy, indx);
      } else {
        if (wildCardCount > 0) {
          let indexOfWild = inputCopy.indexOf("?");
          inputCopy = stripAChar(inputCopy, indexOfWild);
          wildCardCount--;
        } else {
          isMatch = false;
        }
      }
    }
    if (isMatch) {
      results.push(word);
    }
  }

  return sortByLength(results);
};

const stripAChar = (inString, indx) => {
  return (
    inString.substring(0, indx) + inString.substring(indx + 1, inString.length)
  );
};

// sort the results array by length of string
const sortByLength = results => {
  let sendObj = {};

  for (var i = 2; i <= 12; i++) {
    sendObj["" + i] = [];
  }

  for (var result of results) {
    const length = result.length;
    sendObj["" + length].push(result);
  }

  return sendObj;
};

module.exports = {
  alphabetise,
  getWildcardCount,
  checkDictionary
};
