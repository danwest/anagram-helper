"use strict";

const { checkDictionary } = require("./utils");

var express = require("express");
var app = express();
app.listen(3005, () => {
  console.log("Server running on port 3005");
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/anagram/:qcount/:word", (req, res, next) => {
  let wordWithQs = req.params.word;
  const count = req.params.qcount ? req.params.qcount : 0;
  for (var i = 0; i < count; i++) {
    wordWithQs = "?" + wordWithQs;
  }
  const words = checkDictionary(wordWithQs);

  console.log(`Searched for string ${req.params.word}`);

  res.json(JSON.stringify(words));
});
