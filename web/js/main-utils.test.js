"use strict";

const { validateInput } = require("./main-utils");

// input must be 2-12 length and a-z or ?
/////////////////////////////////////////
test("allow length 2-12", () => {
  expect(validateInput("asd?jklajsei")).toBeTruthy();
});

test("disallow length 1", () => {
  expect(validateInput("x")).toBeFalsy();
});

test("disallow length 19", () => {
  expect(validateInput("jhowhfowheofhwoefix")).toBeFalsy();
});

test("disallow null (or empty string)", () => {
  expect(validateInput(null)).toBeFalsy();
});

test("disallow characters that arent't a-z or ?", () => {
  expect(validateInput("sadfjlk$2*")).toBeFalsy();
});

test("allow 2 ?", () => {
  expect(validateInput("elephant??")).toBeTruthy();
});

test("disallow 4 ?", () => {
  expect(validateInput("elep??hant??")).toBeFalsy();
});
