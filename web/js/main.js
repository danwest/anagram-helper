"use strict";

$("#submit-btn").click(() => {
  const inputText = $("#user-word").val();
  const resultsDiv = $("#results-div");
  resultsDiv.text("");

  if (!validateInput(inputText)) {
    resultsDiv.text(
      "Invalid input - needs to be 2-12 characters, at least one a-z, and can include ? for blank pieces."
    );
  } else {
    const wildcardCount = inputText.replace(/[^?]/g, "").length;
    const alpha = inputText.replace(/[?]/g, "");
    const url = "http://localhost:3005/anagram/" + wildcardCount + "/" + alpha;

    $.get(url, data => {
      let parsedData = JSON.parse(data);

      let isWordReturned = false;
      $.each(parsedData, (index, words) => {
        if (words.length > 0) {
          isWordReturned = true;
          let newDiv = $("<div></div>");
          let newTitle = $("<h3>" + index + " letter words</h3>");
          let newList = $("<ul />");
          $.each(words, (index, word) => {
            const newItem = $("<li>" + word + "</li>");
            newList.append(newItem);
          });

          newDiv.append(newTitle).append(newList);

          resultsDiv.prepend(newDiv);
        }
      });

      if (!isWordReturned) {
        resultsDiv.text("No results found.");
      }
    }).fail(() => {
      resultsDiv.text("Error! Please try again.");
    });
  }
});

$("#user-word").on("keydown", event => {
  // allow a-z (65-90), ? (63), backspace (8), enter (13), shift, delete (46), l/r arrows (37, 39)
  if (
    !(
      (event.keyCode >= 65 && event.keyCode <= 90) ||
      event.keyCode == 63 ||
      event.shiftKey ||
      event.keyCode == 8 ||
      event.keyCode == 13 ||
      event.keyCode == 46 ||
      event.keyCode == 37 ||
      event.keyCode == 39
    )
  ) {
    event.preventDefault();
  }
});
