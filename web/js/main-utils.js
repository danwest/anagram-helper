"use strict";

const validateInput = inputString => {
  const valRegex = /^((\?{0}[a-z]{2,12})|(\?{1}[a-z]{1,11})|(\?{2}[a-z]{1,10}))$/i;

  // put question marks at start
  const ordered = alphabetise(inputString);
  return valRegex.test(ordered);
};

const alphabetise = word => {
  if (!word || typeof word == "undefined") {
    return "";
  }
  return word
    .toLowerCase()
    .split("")
    .sort()
    .join("");
};

// jest
if (typeof module !== "undefined") {
  module.exports = {
    validateInput
  };
}
