# Anagram Helper

> Find all anagrams of a string

A Node.js service with a web frontend. The user can input 2-12 characters, either alpha or an optional 1-2 '?' as wildcards.
The service will return all matches of the words found in the dictionary.
The dictionary is taken from [Dave's Word Blog](http://www.tulane.edu/~rice/WB/WB11.HTM)

# Installation

- [npm](https://www.npmjs.com/get-npm)
- [Jest](https://jestjs.io/docs/en/getting-started)
- [Express](https://expressjs.com/en/starter/installing.html)
- [Visual Studio Code](https://code.visualstudio.com/)

Start the app with ```npm start```, and open ```web\index.html``` in your browser.

# Meta

Dan West - danwest2805@gmail.com

Distributed under the MIT license. See [LICENSE.txt](https://gitlab.com/danwest/anagram-helper/blob/master/LICENSE.txt) for more information.

https://gitlab.com/danwest/anagram-helper
